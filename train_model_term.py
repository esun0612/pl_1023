#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 16:47:49 2019

@author: esun

Train term model
"""
import os
from os.path import join as pjoin
import lightgbm as lgb
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')
from util_model import Header, object2cat, read_data_from_header, train_lightgbm, score_model, score_term_model
from metrics import gini, accuracy_index
import re

dat_dir = '/Users/esun/Documents/Experiments/PL_1023/input'
header_file = pjoin(dat_dir, 'term_model_header_PL_1023_remove_tier_loan.csv')
X_train, y_train = read_data_from_header(header_file, pjoin(dat_dir, 'PL_term_train.csv'))
X_oos, y_oos = read_data_from_header(header_file, pjoin(dat_dir, 'PL_term_oos.csv'))
print(X_train.shape, y_train.shape, X_oos.shape, y_oos.shape, X_oot.shape, y_oot.shape)
X_train = X_train.fillna(-99999)
X_oos = X_oos.fillna(-99999)
X_train = object2cat(X_train)
X_oos = object2cat(X_oos)

term_fit_params={
                "early_stopping_rounds":10,
                # "eval_metric" : 'acc', #This sets the objective function, #uses multi_logloss by default
                'eval_names': ['val','train'],
                'verbose': 100,
                'feature_name': 'auto', # that's actually the default
                'categorical_feature': 'auto' # that's actually the default
               }

term_classifier_params={'max_depth':7,
                       'num_leaves': round(.66*(2**7)), # 2/3 of 2^max_depth to avoid overfitting
                       'random_state':12345,
                       'silent':True,
    #                    'metric':'None', # if commented out, early stopping will be enabled either by logloss or auc
                       'n_jobs':4,
                       'n_estimators':10000, # default 100
                       'colsample_bytree':0.75, # default 1
                       'subsample':0.75, # default 1.0
                       'learning_rate':0.05,
                       'boosting_type':'gbdt',
                       'min_child_weight':1e-3,
                       'min_child_samples':20,
                       'reg_alpha':0,
                       'reg_lambda':0,
                       'is_unbalance': False}

model_out_dir = '/Users/esun/Documents/Experiments/PL_1023/model/term_without_tier_loan'
os.makedirs(model_out_dir)
mdl = train_lightgbm(term_fit_params, term_classifier_params, X_train, y_train, X_oos, y_oos, model_out_dir)

#Score model
out_dir = '/Users/esun/Documents/Experiments/PL_1023/output_without_tier_loan'
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols = ['rate_{}'.format(term) for term in PL_terms]
PL_rate_orig_cols = ['rate_{}_orig'.format(term) for term in PL_terms]
model_file = pjoin(model_out_dir, 'lightgbm.txt')

y_pred_train = score_model(pjoin(dat_dir, 'PL_rate_train.csv'), header_file, model_file, pjoin(out_dir, 'term_pred_train.csv'))
y_pred_train_all = score_term_model(pjoin(dat_dir, 'PL_rate_train.csv'), pjoin(out_dir, 'term_pred_train.csv'), 
                                  pjoin(out_dir, 'term_pred_train_final.csv'), 
                                  PL_rate_cols, PL_rate_orig_cols, PL_terms)

y_pred_oos = score_model(pjoin(dat_dir, 'PL_rate_oos.csv'), header_file, model_file, pjoin(out_dir, 'term_pred_oos.csv'))
y_pred_oos_all = score_term_model(pjoin(dat_dir, 'PL_rate_oos.csv'), pjoin(out_dir, 'term_pred_oos.csv'), 
                                  pjoin(out_dir, 'term_pred_oos_final.csv'), 
                                  PL_rate_cols, PL_rate_orig_cols, PL_terms)

y_pred_oot = score_model(pjoin(dat_dir, 'PL_rate_oot.csv'), header_file, model_file, pjoin(out_dir, 'term_pred_oot.csv'))
y_pred_oot_all = score_term_model(pjoin(dat_dir, 'PL_rate_oot.csv'), pjoin(out_dir, 'term_pred_oot.csv'), 
                                  pjoin(out_dir, 'term_pred_oot_final.csv'), 
                                  PL_rate_cols, PL_rate_orig_cols, PL_terms)

def eval_model_perform(df, pred_col, target_col='initial_term'):
    df = df.loc[df[target_col].notnull()]
    gini_result = gini(df[target_col], df[pred_col])
    cm = pd.crosstab(df[target_col], df[pred_col], rownames=['Actual'], colnames=['Predicted'])
    cr_pct = np.diag(cm).sum() / df.shape[0]
    return [gini_result, cr_pct], cm

result_list = [eval_model_perform(df, 'term_max_prob')[0] + eval_model_perform(df, 'term_wgt_prob')[0]
                for df in [y_pred_train_all, y_pred_oos_all, y_pred_oot_all]]
df_result = pd.DataFrame(result_list)   
df_result.columns = ['gini_max', 'cr_pct_max', 'gini_wgt', 'cr_pct_wgt']
df_result.index=['train', 'oos', 'oot']
df_result.to_csv('/Users/esun/Documents/Experiments/PL_1023/model/term_without_tier_loan/metrics_term.csv')

#Compare with SLR
df = pd.read_csv('/Users/esun/Documents/Experiments/SLR_1021/output/final_pred_oot.csv')
eval_model_perform(df, 'term_wgt_prob')
eval_model_perform(df, 'term_max_prob')
