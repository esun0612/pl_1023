#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 10:27:03 2019

@author: esun
"""
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')
from util_data import impute_rate
from util_model import cal_term_rate


cond = ~((df['rate_2'].isnull()) & (df['rate_3'].isnull()) & (df['rate_4'].isnull()) & (df['rate_5'].isnull()) \
         & (df['rate_6'].isnull()) & (df['rate_7'].isnull()))

df = pd.read_csv('/Users/esun/Downloads/louis_test_grid_lgb.csv')
#Impute rate
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols = ['rate_{}'.format(term) for term in PL_terms]
PL_rate_orig_cols = ['rate_{}_orig'.format(term) for term in PL_terms]
prob_cols = ['term{}_xg'.format(term) for term in PL_terms]
prob_cols_adj = ['term{}_xg_adj'.format(term) for term in PL_terms]
df, rate_impute_vals = impute_rate(df, rate_cols=PL_rate_cols)
df = cal_term_rate(df, PL_rate_cols, PL_rate_orig_cols, PL_terms, prob_cols)

def performance_metrics(df, sign_col, rate_col, term_col, term_prob_cols, adj_rate_cols,
                        tier_col='tier', fico_col='credit_score', income_col='gross_income', loan_amt_col='loan_amt'):
    """Output performance metrics
    
    Inputs:
        sign_col: sign indicator
        rate_col: selected apr
        term_col: selected term
        term_prob_cols: list of column names for probability of each term
        adj_rate_cols: imputed rate columns
    """
    population = df.shape[0]
    total_signed = df[sign_col].sum()
    pct_signed = total_signed / population
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
    total_signed_loan_amt = df['signed_loan_amt'].sum()
    conv_dollar_basis = df['signed_loan_amt'].sum() / df[loan_amt_col].sum()
    volume_w_rate = (df[rate_col] * df['signed_loan_amt']).sum()
    wac =  volume_w_rate / total_signed_loan_amt
    wam = (df[term_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    avg_loan_amt = df['signed_loan_amt'].sum() / total_signed
    w_avg_fico = (df[fico_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    w_avg_income = (df[income_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    
    #wac by each term 
    signed_loan_amt_by_term = np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values
    wac_by_term = (df[adj_rate_cols].values * signed_loan_amt_by_term).sum(axis=0) / signed_loan_amt_by_term.sum(axis=0)
    term_dist = signed_loan_amt_by_term.sum(axis=0) / total_signed_loan_amt
    
    #wac by each tier
    wac_by_tier = []
    #Signed loan amount tier distribution
    tier_dist = []
    for tier in range(1, 8):
        df_tier = df.loc[df[tier_col] == tier]
        wac_by_tier.append((df_tier[rate_col] * df_tier['signed_loan_amt']).sum() / df_tier['signed_loan_amt'].sum())
        tier_dist.append(df_tier['signed_loan_amt'].sum() / total_signed_loan_amt)
    
    result = [population, total_signed, pct_signed, conv_dollar_basis, wac, wam, avg_loan_amt, w_avg_fico, w_avg_income] \
             + list(wac_by_term) + list(term_dist) + wac_by_tier + tier_dist
    return result

louis_metrics = performance_metrics(df, 'pred_xg', 'avg_rate', 'term_wgt_prob', prob_cols_adj, PL_rate_cols)
df_result = pd.DataFrame(louis_metrics)   
df_result.index = ['population', 'total_signed', 'pct_signed', 'conv_dollar_basis', 'wac', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income'] + \
                    ['wac_term_{}'.format(term) for term in PL_terms] + ['term_dist_{}'.format(term) for term in PL_terms] + \
                    ['wac_tier_{}'.format(tier) for tier in range(1,8)] + ['tier_dist_{}'.format(tier) for tier in range(1,8)]