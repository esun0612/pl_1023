#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 14:08:22 2019

@author: esun
"""
import os
from os.path import join as pjoin
import numpy as np
import pandas as pd
from util_data import add_signed_ind, bin_yoe, clean_str

dat_dir = '/Users/esun/Documents/Experiments/PL_1023/input'
df = pd.read_csv(pjoin(dat_dir, '18_0101_to_19_0831_PL.csv'))

print(df.shape[0], df[df['initial_term'].notnull()].shape[0], df[df['docs_ind'] == 1].shape[0], df[df['signed_ind'] == 1].shape[0])
df = add_signed_ind(df, terms=[2, 3, 4, 5, 6, 7])
df[['signed_ind_2', 'signed_ind_3', 'signed_ind_4', 'signed_ind_5', 'signed_ind_6', 'signed_ind_7']].sum()
min_start_date = df['date_start'].min()
df['month'] = df['date_start'].dt.year * 12 + df['date_start'].dt.month - (min_start_date.year * 12 + min_start_date.month)
df['month'].value_counts().sort_index()