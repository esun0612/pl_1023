#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 16:45:28 2019

@author: esun

Validate and check the metrics
"""
import os
from os.path import join as pjoin
import lightgbm as lgb
import re
import pandas as pd
import numpy as np
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')

def merge_all_pred(rate_pred_file, term_pred_file, data_file, output_file, act_term_prob_cols, id_col='id',
                   data_cols=['loan_amt', 'gross_income', 'credit_score', 'tier', 'map_interest_rate']):
    """Merge rate, term predictions together and output to output_file
    """
    df_rate_pred = pd.read_csv(rate_pred_file)
    df_term_pred = pd.read_csv(term_pred_file)
    df_data = pd.read_csv(data_file, usecols=[id_col]+data_cols+act_term_prob_cols)
    df_final = pd.merge(df_rate_pred, df_term_pred, on=id_col)
    df_final = pd.merge(df_final, df_data, on=id_col)
    df_final.to_csv(output_file, index=None)
    return df_final

terms = [2, 3, 4, 5, 6, 7]    
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  

dat_dir = '/Users/esun/Documents/Experiments/PL_1023/input'
out_dir = '/Users/esun/Documents/Experiments/PL_1023/output_without_tier_loan'
df_final_train = merge_all_pred(pjoin(out_dir, 'rate_pred_train.csv'), pjoin(out_dir, 'term_pred_train_final.csv'), 
                                pjoin(dat_dir, 'PL_rate_train.csv'), pjoin(out_dir, 'final_pred_train.csv'), act_term_prob_cols)

df_final_oos = merge_all_pred(pjoin(out_dir, 'rate_pred_oos.csv'), pjoin(out_dir, 'term_pred_oos_final.csv'), 
                              pjoin(dat_dir, 'PL_rate_oos.csv'), pjoin(out_dir, 'final_pred_oos.csv'), act_term_prob_cols)

df_final_oot = merge_all_pred(pjoin(out_dir, 'rate_pred_oot.csv'), pjoin(out_dir, 'term_pred_oot_final.csv'), 
                              pjoin(dat_dir, 'PL_rate_oot.csv'), pjoin(out_dir, 'final_pred_oot.csv'), act_term_prob_cols)
print(df_final_train.shape, df_final_oos.shape, df_final_oot.shape)

df_final_train = pd.read_csv(pjoin(out_dir, 'final_pred_train.csv'))
df_final_oos = pd.read_csv(pjoin(out_dir, 'final_pred_oos.csv'))
df_final_oot = pd.read_csv(pjoin(out_dir, 'final_pred_oot.csv'))


def performance_metrics(df, sign_col, rate_col, term_col, term_prob_cols, adj_rate_cols,
                        tier_col='tier', fico_col='credit_score', income_col='gross_income', loan_amt_col='loan_amt'):
    """Output performance metrics
    
    Inputs:
        sign_col: sign indicator
        rate_col: selected apr
        term_col: selected term
        term_prob_cols: list of column names for probability of each term
        adj_rate_cols: imputed rate columns
    """
    population = df.shape[0]
    total_signed = df[sign_col].sum()
    pct_signed = total_signed / population
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
    total_signed_loan_amt = df['signed_loan_amt'].sum()
    conv_dollar_basis = df['signed_loan_amt'].sum() / df[loan_amt_col].sum()
    volume_w_rate = (df[rate_col] * df['signed_loan_amt']).sum()
    wac =  volume_w_rate / total_signed_loan_amt
    wam = (df[term_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    avg_loan_amt = df['signed_loan_amt'].sum() / total_signed
    w_avg_fico = (df[fico_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    w_avg_income = (df[income_col] * df['signed_loan_amt']).sum() / total_signed_loan_amt
    
    #wac by each term 
    signed_loan_amt_by_term = np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values
    wac_by_term = (df[adj_rate_cols].values * signed_loan_amt_by_term).sum(axis=0) / signed_loan_amt_by_term.sum(axis=0)
    term_dist = signed_loan_amt_by_term.sum(axis=0) / total_signed_loan_amt
    
    #wac by each tier
    wac_by_tier = []
    #Signed loan amount tier distribution
    tier_dist = []
    for tier in range(1, 8):
        df_tier = df.loc[df[tier_col] == tier]
        wac_by_tier.append((df_tier[rate_col] * df_tier['signed_loan_amt']).sum() / df_tier['signed_loan_amt'].sum())
        tier_dist.append(df_tier['signed_loan_amt'].sum() / total_signed_loan_amt)
    
    result = [population, total_signed, pct_signed, conv_dollar_basis, wac, wam, avg_loan_amt, w_avg_fico, w_avg_income] \
             + list(wac_by_term) + list(term_dist) + wac_by_tier + tier_dist
    return result

def report_performance_metrics(dfs, df_names, terms, act_term_prob_cols, pred_term_prob_cols, adj_rate_cols, 
                               pred_sign_col='rate_pred', pred_rate_col='avg_rate', pred_term_col='term_wgt_prob',
                               act_sign_col='signed_ind', act_rate_col='map_interest_rate', act_term_col='initial_term'):
    """Report the performance metrics for given dfs (list of dataframes) and corresponding data frame names in list (df_names) for prediction and actual
    """
    final_result = []
    for df in dfs:
        df.dropna(subset=pred_term_prob_cols, inplace=True) #Should filter out observations with all rates nan in the beginning
        final_result.append(performance_metrics(df, act_sign_col, act_rate_col, act_term_col, act_term_prob_cols, adj_rate_cols))
        final_result.append(performance_metrics(df, pred_sign_col, pred_rate_col, pred_term_col, pred_term_prob_cols, adj_rate_cols))
    df_result = pd.DataFrame(final_result)   
    df_result.columns = ['population', 'total_signed', 'pct_signed', 'conv_dollar_basis', 'wac', 'wam', 'avg_loan_amt', 'w_avg_fico', 'w_avg_income'] + \
                        ['wac_term_{}'.format(term) for term in terms] + ['term_dist_{}'.format(term) for term in terms] + \
                        ['wac_tier_{}'.format(tier) for tier in range(1,8)] + ['tier_dist_{}'.format(tier) for tier in range(1,8)]
    df_result.index = [df_name + '_' + seg for df_name in df_names for seg in ['act', 'pred']] 
    return df_result

terms = [2, 3, 4, 5, 6, 7]
rate_cols = ['rate_{}'.format(term) for term in terms]
pred_term_prob_cols = ['rate_{}_prob_adj'.format(term) for term in terms]     
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
   
final_result = report_performance_metrics([df_final_train, df_final_oos, df_final_oot], ['train', 'oos', 'oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_cols)
final_result.to_csv('/Users/esun/Documents/Experiments/PL_1023/final_metrics_without_tier_loan.csv')


#Validate OOT AA grid
df_final_oot = pd.read_csv(pjoin(out_dir, 'final_pred_oot.csv'))
df_grid = pd.read_csv(pjoin(dat_dir, '18_0101_to_19_0930_PL_grid.csv'))
df_final_oot = pd.merge(df_final_oot, df_grid, on='id')
df_oot_AA = df_final_oot[df_final_oot['challenger_name'] == 'O.G. Champion']

final_result = report_performance_metrics([df_oot_AA], ['oot_AA'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_cols)
final_result.to_csv('/Users/esun/Documents/Experiments/PL_1023/final_metrics_oot_AA_without_tier_loan.csv')