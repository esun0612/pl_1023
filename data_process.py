#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 10:32:30 2019

@author: esun
Preprocess data for rate model
Term model preprocessing step does exactly the same
"""

import os
from os.path import join as pjoin
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')
from util_data import add_signed_ind, bin_yoe, clean_str, divide_train_oos, map_term_to_rate, impute_rate

def handle_loan_amt(df):
    df['used_loan_amt'] = np.min(df[['loan_amt', 'max_eligible_amt']], axis=1) # if amount requested is over max eligible amount, show max eligible amount
    df['used_vs_req_loan_amt_diff'] = df['used_loan_amt'] - df['loan_amt'] # negative if amount requested is higher than what was given, else 0.
    return df

def data_preprocessing(df_in):
    """Add month and day columns, bucket years_of_experience and categorize attr_affiliate_referrer
    """
    df = df_in.copy()
    df = df.loc[df['loan_amt'] != 0]
    df['years_of_experience_bins'] = df['years_of_experience'].map(lambda x: bin_yoe(x)) # put years of experience into bins
    df['attr_affiliate_referrer_new'] = clean_str(df['attr_affiliate_referrer'])
    df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer_new'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                 df['attr_affiliate_referrer_new'], 'other')
    df['member_indicator'] = np.where(df['member_indicator'] is True, 1, 0)
    df = handle_loan_amt(df)
    return df


dat_dir = '/Users/esun/Documents/Experiments/PL_1023/input'
df = pd.read_csv(pjoin(dat_dir, '18_0101_to_19_0930_PL.csv'))
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols = ['rate_{}'.format(term) for term in PL_terms]
PL_rate_maps = {term: idx for idx, term in enumerate(PL_terms)}
df = add_signed_ind(df, terms=PL_terms)
df = data_preprocessing(df)
df, rate_impute_vals = impute_rate(df, rate_cols=PL_rate_cols)
df['map_interest_rate'] = df.apply(lambda x: map_term_to_rate(x, rate_cols=PL_rate_cols, terms=PL_terms, rate_map=PL_rate_maps), axis=1)
min_start_date = df['date_start'].min()
df['month'] = df['date_start'].dt.year * 12 + df['date_start'].dt.month - (min_start_date.year * 12 + min_start_date.month)
df.to_csv(pjoin(dat_dir, '18_0101_to_19_0831_PL_pp.csv'), index=None)
df.iloc[:100].to_csv(pjoin(dat_dir, 'PL_sample.csv'), index=None)

df = df.loc[df['date_start'] <= '2019-09-23']
#df['month'].value_counts().sort_index()
df_oot = df.loc[df['month'] >= 19]
df_train, df_oos = divide_train_oos(df.loc[df['month'] < 19], random_state=612)
print(df_train.shape, df_oos.shape, df_oot.shape)
print(df_train.shape[0] / df.shape[0], df_oos.shape[0] / df.shape[0], df_oot.shape[0] / df.shape[0])
df_train.to_csv(pjoin(dat_dir, 'PL_rate_train.csv'), index=None)
df_oos.to_csv(pjoin(dat_dir, 'PL_rate_oos.csv'), index=None)
df_oot.to_csv(pjoin(dat_dir, 'PL_rate_oot.csv'), index=None)

#Select observations with non-missing "initial_term" variable to be used in term prediction model
df_train_term = df_train[df_train['initial_term'].notnull()]
df_oos_term = df_oos[df_oos['initial_term'].notnull()]
df_oot_term = df_oot[df_oot['initial_term'].notnull()]
print(df_train_term.shape, df_oos_term.shape, df_oot_term.shape)
print(df_train_term.shape[0] / df.shape[0], df_oos_term.shape[0] / df.shape[0], df_oot_term.shape[0] / df.shape[0])
df_train_term.to_csv(pjoin(dat_dir, 'PL_term_train.csv'), index=None)
df_oos_term.to_csv(pjoin(dat_dir, 'PL_term_oos.csv'), index=None)
df_oot_term.to_csv(pjoin(dat_dir, 'PL_term_oot.csv'), index=None)


