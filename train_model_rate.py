#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 13:58:06 2019

@author: esun

Train rate model

#Exp 1: remove tier variable
"""
import os
from os.path import join as pjoin
import lightgbm as lgb
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')
from util_model import Header, object2cat, read_data_from_header, train_lightgbm
from metrics import gini, accuracy_binary

dat_dir = '/Users/esun/Documents/Experiments/PL_1023/input'
header_file = pjoin(dat_dir, 'rate_model_header_PL_1023_remove_tier_loan.csv')
X_train, y_train = read_data_from_header(header_file, pjoin(dat_dir, 'PL_rate_train.csv'))
X_oos, y_oos = read_data_from_header(header_file, pjoin(dat_dir, 'PL_rate_oos.csv'))
X_oot, y_oot = read_data_from_header(header_file, pjoin(dat_dir, 'PL_rate_oot.csv'))
print(X_train.shape, y_train.shape, X_oos.shape, y_oos.shape, X_oot.shape, y_oot.shape)
X_train = X_train.fillna(-99999)
X_oos = X_oos.fillna(-99999)
X_oot = X_oot.fillna(-99999)
X_train = object2cat(X_train)
X_oos = object2cat(X_oos)
X_oot = object2cat(X_oot)

#Train lightgbm model
fit_params={"early_stopping_rounds":20,
                "eval_metric" : 'auc',
                'eval_names': ['val','train'],
                'verbose': 100,
                'feature_name': 'auto', # that's actually the default
                'categorical_feature': 'auto' # that's actually the default
               }

classifier_params={'num_leaves': 15, # default 31
                       'max_depth':-1,
                       'random_state':12345,
                       'silent':True,
                    #    'metric':'None', # if commented out, early stopping will be enabled either by logloss or auc
                       'n_jobs':4,
                       'n_estimators':1000, # default 100
                       'colsample_bytree':0.9, # default 1
                       'subsample':0.9, # default 1.0
                       'learning_rate':0.1,
                       'boosting_type':'gbdt',
                       'min_child_weight':1e-3,
                       'min_child_samples':20,
                       'reg_alpha':0,
                       'reg_lambda':0,
                    #    'use_missing':False #setting this to False improves our # signed predictions
                       }

model_out_dir = '/Users/esun/Documents/Experiments/PL_1023/model/rate_without_tier_loan'
os.makedirs(model_out_dir)
PL_terms = [2, 3, 4, 5, 6, 7]
PL_rate_cols = ['rate_{}'.format(term) for term in PL_terms]
mdl = train_lightgbm(fit_params, classifier_params, X_train, y_train, X_oos, y_oos, 
                     model_out_dir, monotone_cols=PL_rate_cols)

def score_rate_model(data_file, X, model, output_file, id_col='id', target_col='signed_ind'):
    y_pred = mdl.predict_proba(X)[:,1]
    df = pd.read_csv(data_file, usecols=[id_col, target_col])
    df['rate_pred'] = y_pred
    df.to_csv(output_file, index=None)
    return df

out_dir = '/Users/esun/Documents/Experiments/PL_1023/output_without_tier_loan'
os.makedirs(out_dir)
df_rate_train = score_rate_model(pjoin(dat_dir, 'PL_rate_train.csv'), X_train, mdl, pjoin(out_dir, 'rate_pred_train.csv'))
df_rate_oos = score_rate_model(pjoin(dat_dir, 'PL_rate_oos.csv'), X_oos, mdl, pjoin(out_dir, 'rate_pred_oos.csv'))
df_rate_oot = score_rate_model(pjoin(dat_dir, 'PL_rate_oot.csv'), X_oot, mdl, pjoin(out_dir, 'rate_pred_oot.csv'))

df_rate_train = pd.read_csv(pjoin(out_dir, 'rate_pred_train.csv'))
df_rate_oos = pd.read_csv(pjoin(out_dir, 'rate_pred_oos.csv'))
df_rate_oot = pd.read_csv(pjoin(out_dir, 'rate_pred_oot.csv'))

gini_train = gini(df_rate_train['signed_ind'], df_rate_train['rate_pred'])
gini_oos = gini(df_rate_oos['signed_ind'], df_rate_oos['rate_pred'])
gini_oot = gini(df_rate_oot['signed_ind'], df_rate_oot['rate_pred'])
acc_train = accuracy_binary(df_rate_train['signed_ind'], df_rate_train['rate_pred'])
acc_oos = accuracy_binary(df_rate_oos['signed_ind'], df_rate_oos['rate_pred'])
acc_oot = accuracy_binary(df_rate_oot['signed_ind'], df_rate_oot['rate_pred'])

df_result = pd.DataFrame({'gini': [gini_train, gini_oos, gini_oot], 
                         'accuracy': [acc_train, acc_oos, acc_oot]},
                        index=['train', 'oos', 'oot'])
df_result.to_csv(pjoin(model_out_dir, 'rate_metrics.csv'))

"""
y_train_pred = mdl.predict_proba(X_train)[:,1]
y_oos_pred = mdl.predict_proba(X_oos)[:,1]
y_oot_pred = mdl.predict_proba(X_oot)[:,1]
gini_train = gini(y_train.iloc[:, 0], y_train_pred)
gini_oos = gini(y_oos.iloc[:, 0], y_oos_pred)
gini_oot = gini(y_oot.iloc[:, 0], y_oot_pred)
acc_train = accuracy_index(y_train.iloc[:, 0], y_train_pred)
acc_oos = accuracy_index(y_oos.iloc[:, 0], y_oos_pred)
acc_oot = accuracy_index(y_oot.iloc[:, 0], y_oot_pred)
"""




