#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:44 2019

@author: esun

Utility functions for metrics
"""
import numpy as np
import pandas as pd
from sklearn import metrics

def auc_to_gini(auc):
    """Convert ROC AUC to gini"""
    return 2 * auc - 1

def _unscaled_gini(true, pred):
    """Unscaled Gini calculation for continuous variables"""
    df = pd.DataFrame({'true':true, 'pred':pred})
    true_min = df['true'].min()
    if true_min < 0:
        df['true'] = df['true'] - true_min 
        df['pred'] = df['pred'] - true_min 
    total = df['true'].sum()
    y = df.sort_values('pred', ascending=False)['true'].cumsum().values / total
    trap = (2 * y.sum() - y[-1]) / (2 * df.shape[0])
    return trap - 1/2

def gini(true, pred):
    """Compute model gini"""
    try:
        return auc_to_gini(metrics.roc_auc_score(y_true=true, y_score=pred))
    except ValueError:
        return _unscaled_gini(true, pred) / _unscaled_gini(true, true)

def accuracy_binary(true, pred):
    """Evaluate accuracy for classification problem
    """
    df = pd.DataFrame({'true': true, 'pred': pred})
    return 1 - np.abs(df['pred'] - df['true']).sum() / df[df['true'].notnull()].shape[0]    
    
def accuracy_index(true, pred, wgt=None, bins=10, cont=True):
    """
    Calculate the accuracy index with weight
    If wgt is not specified, default wgt is set to one, i.e. no weight
    cont is the indicator for continuous or discrete
    """
    df = pd.DataFrame({'true': true, 'pred': pred})
    if wgt is None:
        df['wgt'] = 1
    else:
        df['wgt'] = wgt
    df = df.sort_values(by='pred', ascending=False)
    df['true'] = df['true'] * df['wgt']
    df['pred'] = df['pred'] * df['wgt']      
    if cont:
        try:
            accuracy = 1 - np.abs(df['pred'] - df['true']).sum() / np.abs(df['true']).sum()
            return accuracy
        except ZeroDivisionError:
            return np.nan
    else:
        df['bin'] = np.floor(df['wgt'].cumsum() / df['wgt'].sum() * bins) + 1
        df['bin'] = np.where(df['bin'] > bins, bins, df['bin'])
        df_agg = df.groupby('bin').agg({'bin': 'count', 'true': 'sum', 'pred': 'sum'})
        try:
            accuracy = 1 - np.abs(df_agg['pred'] - df_agg['true']).sum() / np.abs(df_agg['true']).sum()
            return accuracy
        except ZeroDivisionError:
            return np.nan
