#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:59 2019

@author: esun

Utility functions for model
"""
import os
from os.path import join as pjoin
import re
import numpy as np
import pandas as pd
import lightgbm as lgb
import matplotlib.pyplot as plt

class Header():
    """Helper class for header file
    """
    def __init__(self, header_file):
        self.header = pd.read_csv(header_file)
        self.cols = list(self.header.columns)
        self.types = list(self.header.loc[0])
        self.index = list(range(0, len(self.cols)))
        self.num_cols = [x for x, y in zip(self.cols, self.types) if y == 'N']
        self.num_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'N']
        self.cat_cols = [x for x, y in zip(self.cols, self.types) if y == 'C']
        self.cat_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'C']
        self.key_cols = [x for x, y in zip(self.cols, self.types) if y == 'K']
        self.key_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'K']
        self.label_cols = [x for x, y in zip(self.cols, self.types) if y == 'L']
        self.label_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'L']
        self.ignore_cols = [x for x, y in zip(self.cols, self.types) if y == 'I']
        self.ignore_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'I']
        

def object2cat(X):
    """Cast categorical columns to type 'category'
    """
    for c in X.columns:
        col_type = X[c].dtype
        if col_type == 'object' or col_type.name == 'category':
            X[c] = X[c].astype('category')
    return X

def read_data_from_header(header_file, data_file):
    """Based on data_file and header_file, return X(training variables) and y (label)
    """
    header = Header(header_file)
    df = pd.read_csv(data_file)
    X = df[header.num_cols + header.cat_cols]
    y = df[header.label_cols]
    return X, y

def missing_impute(df, header_file):
    """Impute by missing values specified in header_file
    Will implement in the future
    """
    return

def train_lightgbm(fit_params, classifier_params, X_train, y_train, X_oos, y_oos, output_folder,
                   monotone_cols=None):
    """Train lightgbm
    """
    #Evaluation datasets used in fit early stopping
    fit_params['eval_set'] = [(X_train, y_train), (X_oos, y_oos)]
    
    #Define monotone features in model
    if monotone_cols is not None:
        constrs = [0 for x in range(len(X_train.columns))]
        for rate_col in monotone_cols:
            rate_col_ind = list(X_train.columns).index(rate_col)
            constrs[rate_col_ind] = -1
        classifier_params['monotone_constraints'] = constrs 

    ## lightgbm sklearn model
    clf = lgb.LGBMClassifier(**classifier_params)
    clf.fit(X_train, y_train, **fit_params)

    # plot metrics
    lgb.plot_metric(clf)
    plt.savefig(pjoin(output_folder, 'metric_plot.png'), bbox_inches='tight')
    for fi in ['split','gain']:
        lgb.plot_importance(clf, title=f'Feature Importance by {fi}', importance_type=fi,max_num_features=100,figsize=(10,14))
        plt.savefig(pjoin(output_folder, f'importance_plot_{fi}.png'), bbox_inches='tight')
    clf.booster_.save_model(pjoin(output_folder, 'lightgbm.txt'))
    return clf

def score_model(data_file, header_file, model_file, output_file):
    """Score rate model and output id_col, prediction and target_col
    
    Input:
        data_file: original data file for prediction
        header_file: header file indicating column type
        model_file: pre-trained model file
        output_file: location to save output data
    """
    X, y = read_data_from_header(header_file, data_file)
    X = X.fillna(-99999)
    X = object2cat(X)
    mdl = lgb.Booster(model_file=model_file)
    y_pred = pd.DataFrame(mdl.predict(X))
    y_pred.to_csv(output_file, index=None)
    return y_pred

def score_term_model(data_file, raw_model_output_file, output_file, 
                     impute_rate_cols, orig_rate_cols, terms,
                     id_col='id', target_col='initial_term'):
    """From raw prediction populate the final columns
    
    Input:
        data_file: original data file for prediction
        raw_model_output_file: raw prediction from lightgbm
        output_file: location to save output data
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    p = pd.read_csv(raw_model_output_file)
    prob_cols = [rate_col + '_prob' for rate_col in impute_rate_cols]
    p.columns = prob_cols
    df = pd.read_csv(data_file, usecols=[id_col, target_col] + impute_rate_cols + orig_rate_cols)
    df = pd.merge(df, p, left_index=True, right_index=True)
    df = cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols)
    df.to_csv(output_file, index=None)
    return df

def cal_term_rate(df, impute_rate_cols, orig_rate_cols, terms, prob_cols):
    """Generate the following:
        1. term with highest score
        2. score weighted average term and round to the nearest term in terms
        3. score weighted average rate
        
    Input: 
        df: original data frame and raw prediction
        impute_rate_cols: list for column names of rates with missing imputations
        orig_rate_cols: list for column names of rates without missing imputations
        terms: list of integer terms
    """
    rates = df[orig_rate_cols]
    #mask matrix: 0 for missing rates, 1 non-missing
    mask = ~rates.isna() + 0
    adjusted_p = df[prob_cols].values * mask.values
    adjusted_p = pd.DataFrame(adjusted_p)
    
    #1. term with highest score
    adjusted_p_max = adjusted_p.idxmax(axis=1) #index for the maximum probability
    term_map = {idx: term for idx, term in enumerate(terms)}
    df['term_max_prob'] = adjusted_p_max.map(term_map) #map the index to rate
        
    #2. score weighted average term and round to the nearest term in terms
    adjusted_p['sum'] = adjusted_p.sum(axis=1)
    for col in adjusted_p.columns[: -1]:
        adjusted_p[col] = adjusted_p[col] / adjusted_p['sum']
    term_p_cols = []
    for idx, term in enumerate(terms):
        term_p_col_nm = '{}_p'.format(term)
        adjusted_p[term_p_col_nm] = adjusted_p[idx] * term
        term_p_cols.append(term_p_col_nm)
    df['term_wgt_prob'] = adjusted_p[term_p_cols].sum(axis=1)
    
    def find_closest_value(val):
        distance = 999999
        answer = -1
        for term in terms:
            if np.abs(val-term) < distance:
                distance = np.abs(val-term)
                answer = term
            else:
                continue
        return answer 
    
    df['term_wgt_prob'] = df['term_wgt_prob'].map(find_closest_value)
    
    #Merge adjusted probability with df
    df = pd.merge(df, adjusted_p.iloc[:, :len(terms)], left_index=True, right_index=True)
    prob_cols_adj = [prob_col + '_adj' for prob_col in prob_cols]
    df.rename(columns={idx: prob_col_adj for idx, prob_col_adj in enumerate(prob_cols_adj)}, inplace=True)
    
    #3. score weighted average rate  
    df['avg_rate'] = np.sum(df[impute_rate_cols].values * df[prob_cols_adj].values, axis=1)
    return df

