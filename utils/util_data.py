#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 13:13:26 2019

@author: esun

Utility functions for data query and processing
"""

from sqlalchemy import create_engine
import numpy as np
import pandas as pd

def query_sofidw(qry):
    """Return the pandas DataFrame from input SQL qry with connection to sofidw
    """
    my_db = 'postgresql://localhost:15501/dw-prod-sofidw-ro'
    engine = create_engine(my_db, echo=True)
    df = pd.read_sql_query(qry, con=engine)
    return df

def divide_train_oos(df, random_state=None, frac=0.7):
    """Divide df into frac for training and 1 - frac for oos data
    """
    train = df.sample(frac=0.7, random_state=random_state)
    test = df.drop(train.index)
    return train, test

def add_signed_ind(df, model_target='signed_ind', term_var='initial_term', terms=None, filterdays = 30):
    """Set the signed indicator by terms and filter out the ones with signed timewindow > filterdays
    Also change target variable accordingly by timewindow
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['date_signed'] = pd.to_datetime(df['date_signed'])
    if terms is None: 
        #Infer terms list from "initial_term" variable
        terms = df['initial_term'].unique() #take distinct term values as a numpy array
        terms = sorted(terms[~np.isnan(terms)].astype(int))
    for term in terms:
        df[model_target + '_{0}'.format(term)] = np.where((df[model_target] == 1) & (df[term_var] == term) & (df['date_signed'] - df['date_start'] <= pd.Timedelta(filterdays,'D')), 1, 0)
    df[model_target] = np.where(df['date_signed'] - df['date_start'] > pd.Timedelta(filterdays,'D'), 0, df[model_target])
    return df

def bin_yoe(x):
    """Bucket years of experience
    """
    if pd.isnull(x):
        return 'null'
    elif x == 0:
        return '0'
    elif x <= 5:
        return '1_to_5'
    elif x <= 10:
        return '6_to_10'
    else:
        return 'over_10'
    
def clean_str(str_col):
    """Return lower case of str_col and replace ' ' and '-' with '_'
    """
    str_col = str_col.str.lower()
    str_col = str_col.str.replace(" ",'_')
    str_col = str_col.str.replace("-",'_')
    return str_col

def map_term_to_rate(x, term_col='initial_term', rate_cols=['rate_5','rate_7','rate_10','rate_15','rate_20'], terms=[5, 7, 10, 15, 20],
                     rate_map={5: 0, 7: 1, 10: 2, 15: 3, 20: 4}):
    """Read interest rate from rate_cols based on 'initial_term' selected by the customer
    """
    term = x[term_col]
    if np.isnan(term):
        return np.nan
    else:
        rate_cols_idx = rate_map[term]
        return x[rate_cols[rate_cols_idx]]
    
def impute_rate(df, rate_cols=['rate_5','rate_7','rate_10','rate_15','rate_20'], impute_rate=None, keep_orig=True):
    """Impute rate_cols by maximum of rate_cols if impute_rate is None, else by impute_rate dictionary
    """
    if impute_rate is None:
        impute_rate = df[rate_cols].max().to_dict()
    if keep_orig:
        for rate_col in rate_cols:
            df[rate_col + '_orig'] = df[rate_col]
    df[rate_cols] = df[rate_cols].fillna(value=impute_rate)
    return df, impute_rate