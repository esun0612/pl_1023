#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 09:25:08 2019

@author: esun
Query basic data from sofidw
"""
import pandas as pd
import sys
sys.path.insert(0, '/Users/esun/Documents/erika_git/PL_1023/utils')
from util_data import query_sofidw

start_date = '2018-01-01'
end_date = '2019-09-30'
product = f"'PL'" #'REFI' for SLR, 'PL' for personal loan
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

df_query = query_sofidw(PL_qry)
df_query.to_csv('/Users/esun/Documents/Experiments/PL_1023/input/18_0101_to_19_0930_PL.csv', index=None)

#Pull data for stu attributes
start_date = '2019-08-01'
end_date = '2019-09-23'
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"
PL_qry_stu_4 =  f"""
SELECT
af.id,
acaf.stu0416, 
acaf.stu2550, 
acaf.stu6200, 
acaf.stu8151
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""
df_query_stu_4 = query_sofidw(PL_qry_stu_4)
df = pd.read_csv('/Users/esun/Documents/Experiments/PL_1023/input/to_louis_oot.csv')
df2 = pd.merge(df, df_query_stu_4, on='id', how='left')
df2.to_csv('/Users/esun/Documents/Experiments/PL_1023/input/to_louis_oot.csv', index=None)

#Pull data for grid
PL_qry_grid =  f"""
SELECT
af.id,
af.challenger_name
FROM dwmart.applications_file af
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""
df_query_grid = query_sofidw(PL_qry_grid)
df_query_grid.to_csv('/Users/esun/Documents/Experiments/PL_1023/input/18_0101_to_19_0930_PL_grid.csv', index=None)

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
round(af.requested_amount, -3)   AS loan_amt,
round(af.gross_income, -3)       AS gross_income,
CASE WHEN af.g_program IS NOT NULL
THEN 1
ELSE 0 END                       AS grad,
af.credit_score,
oo.rate_2,
oo.rate_3,
oo.rate_4,
oo.rate_5,
oo.rate_6,
oo.rate_7,
coalesce(af.tier :: int, oo.tier) AS tier,
oo.max_eligible_amt,
CASE WHEN af.coborrower_applicant_id IS NOT NULL THEN 1 ELSE 0 END AS coborrower_ind,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.monthly_housing_cost,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.ILN0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820 ,
acaf.all5820,
acaf.all6200,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.iqf9416,
acaf.iqt9415,
acaf.rev2800,
acaf.rev5020,
acaf.rev6200,
acaf.rev8150,
acaf.rev8320,
acaf.rta7110,
acaf.rta7300,
acaf.rtr5030,
acaf.rtr6280,
acaf.all9110,
acaf.all6160,
acaf.all7517,
acaf.all8154,
acaf.all8163,
acaf.all9951,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
round(af.free_cash_flow_pre, -2) AS fcf_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
LEFT JOIN
(
SELECT
paf.application_id,
min(CASE WHEN p.product_term = 2
THEN of.min_rate END) AS rate_2,
min(CASE WHEN p.product_term = 3
THEN of.min_rate END) AS rate_3,
min(CASE WHEN p.product_term = 4
THEN of.min_rate END) AS rate_4,
min(CASE WHEN p.product_term = 5
THEN of.min_rate END) AS rate_5,
min(CASE WHEN p.product_term = 6
THEN of.min_rate END) AS rate_6,
min(CASE WHEN p.product_term = 7
THEN of.min_rate END) AS rate_7,
min(SUBSTRING(o.min_tier_code,'[1-9]')::INT) AS tier,
max(of.max_amount) AS max_eligible_amt
FROM product_application_facts paf
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
GROUP BY paf.application_id
) oo ON oo.application_id = af.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL) \
AND af.current_decision = 'ACCEPT'
"""



